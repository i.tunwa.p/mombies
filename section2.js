for (var i = 1; i <= 100; i++) {

	if (i%3 == 0 && i%5 == 0) console.log("FizzBuzz");
	else if (i%3 == 0) console.log("Fizz");
	else if (i%5 == 0) console.log("Buzz");
	else console.log(i);
}

/*--------end of 2.1--------*/

var size = 8; //chessboard's size (should be even number)
var chessBoard = "";
var temp = "";

for (var i = 0; i < size; i++) { //to create chessboard string
	for (var j = 0; j< size; j++) {
		if ((i+j)%2 == 0) chessBoard+=" ";
		else chessBoard+="#";
	}
}
for (var i = 1; i <= chessBoard.length; i++) { //to print each line of the board
	temp+=chessBoard.charAt(i-1);
	if (i%(size) == 0) {
		console.log(temp);
		temp = "";
	}
}

/*-------end of 2.2---------*/

function range(begin,end,step) {

	var array = [];
	if (step == null) step = 1;
	if (begin == end) array.push(begin);
	else if (begin < end && step > 0) {
		for (var i = begin; i <= end; i+=step) array.push(i);
	}
	else if (begin > end && step < 0) {
		for (var i = begin; i >= end; i+=step) array.push(i);
	}
	return array;
}

function sum(array) {

	console.log("sum of ", array, "is ");
	var sum = 0;
	for (var i = 0; i < array.length; i++) sum+=parseInt(array[i]);
	return sum;
}

console.log(sum(range(10,1,-2)));
