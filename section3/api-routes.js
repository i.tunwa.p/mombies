let router = require('express').Router();

router.get('/', function (req, res) {
    res.json({
        status: 'API is Working',
        message: 'Welcome',
    });
});

var bookController = require('./authors/authorController');
var authorController = require('./authors/authorController');

router.route('/books')
    .get(bookController.index)
    .post(bookController.new);
router.route('/books/:book_id')
    .get(bookController.view)
    .patch(bookController.update)
    .put(bookController.update)
    .delete(bookController.delete);
router.route('/authors')
    .get(authorController.index)
    .post(authorController.new);
router.route('/authors/:author_id')
    .get(authorController.view)
    .patch(authorController.update)
    .put(authorController.update)
    .delete(authorController.delete);
// Export API routes
module.exports = router;