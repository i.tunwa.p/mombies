var mongoose = require('mongoose');
// Setup schema
var bookSchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    summary: String,
    ISBN: String,
    genre: String,
    url: String,
});

var Book = module.exports = mongoose.model('Book', bookSchema);
module.exports.get = function (callback, limit) {
    Book.find(callback).limit(limit);
}