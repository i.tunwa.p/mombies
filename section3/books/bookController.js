Book = require('./bookModel');

exports.index = function (req, res) {
    console.log("doing see all");
    
    Book.get(function (err, books) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Books retrieved successfully",
            data: books
        });
    });
};

exports.new = function (req, res) {
    console.log("doing create new", req.body);
    var book = new Book();
    book.title = req.body.title;
    book.author = req.body.author;
    book.summary = req.body.summary;
    book.ISBN = req.body.ISBN;
    book.genre = req.body.genre;
    book.url = req.body.url;

    book.save(function (err) {
        if (err)
             res.json(err);
        res.json({
            message: 'New book created!',
            data: book
        });
    });
};
exports.view = function (req, res) {
    console.log("doing see one");
    Book.findById(req.params.book_id, function (err, book) {
        if (err)
            res.send(err);
        res.json({
            message: 'Book details loading..',
            data: book
        });
    });
};
exports.update = function (req, res) {
    console.log("doing update one");
    Book.findById(req.params.book_id, function (err, book) {
        if (err)
            res.send(err);
        book.title = req.body.title;
        book.author = req.body.author;
        book.summary = req.body.summary;
        book.ISBN = req.body.ISBN;
        book.genre = req.body.genre;
        book.url = req.body.url;

        book.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Book Info updated',
                data: book
            });
        });
    });
};

exports.delete = function (req, res) {
    console.log("doing delete one");
    Book.remove({
        _id: req.params.book_id
    }, function (err, book) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Book deleted'
        });
    });
};