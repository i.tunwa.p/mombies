var mongoose = require('mongoose');
// Setup schema
var authorSchema = mongoose.Schema({
    first_name: String,
    family_name: String,
    date_of_birth: Date,
    date_of_death: Date,
    name: {
        type: String,
        required: true,
    },
    lifespan: Number,
    url: String,
});

var Author = module.exports = mongoose.model('Author', authorSchema);
module.exports.get = function (callback, limit) {
    Author.find(callback).limit(limit);
}