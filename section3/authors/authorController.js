Author = require('./authorModel');

exports.index = function (req, res) {
    console.log("doing see all");

    Author.get(function (err, authors) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Authors retrieved successfully",
            data: authors
        });
    });
};

exports.new = function (req, res) {
    console.log("doing create new", req.body);
    var author= new Author();
    author.first_name = req.body.first_name;
    author.family_name = req.body.family_name;
    author.date_of_birth = req.body.date_of_birth;
    author.date_of_death = req.body.date_of_death;
    author.name = req.body.name;
    author.lifespan = req.body.lifespan;
    author.url = req.body.url;

    author.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New author created!',
            data: author
        });
    });
};
exports.view = function (req, res) {
    console.log("doing see one");
    Author.findById(req.params.author_id, function (err, author) {
        if (err)
            res.send(err);
        res.json({
            message: 'Author details loading..',
            data: author
        });
    });
};
exports.update = function (req, res) {
    console.log("doing update one");
    Author.findById(req.params.author_id, function (err, author) {
        if (err)
            res.send(err);
        author.first_name = req.body.first_name;
        author.family_name = req.body.family_name;
        author.date_of_birth = req.body.date_of_birth;
        author.date_of_death = req.body.date_of_death;
        author.name = req.body.name;
        author.lifespan = req.body.lifespan;
        author.url = req.body.url;

        author.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Author Info updated',
                data: author
            });
        });
    });
};

exports.delete = function (req, res) {
    console.log("doing delete one");
    Author.remove({
        _id: req.params.author_id
    }, function (err, author) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Author deleted'
        });
    });
};