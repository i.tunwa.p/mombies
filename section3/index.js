let express = require('express')
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let apiRoutes = require("./api-routes")

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/api', apiRoutes)

mongoose.connect('mongodb://localhost/section3');

var port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('Section 3'));

app.listen(port, function () {
    console.log("Server is on port : " + port);
});